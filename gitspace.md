A game parasitizing on github/lab/notabug servers.
Engine+levels model.
Can be either 3d or 2d (though i want 3d)

levels are hosted on git in different repos (from different people) and are dynamically loaded and maybe even compiled.
levels are connected by doors which trigger git shallow cloning.
Doors have lights which change color when ready.
On some of them player can select what level to load.
Levels are nested into sth like Submachine coordinate system instead of url addresses (though url is ok for debug)
Choosing looks like a terminal near the door.

Choosing can be restricted, and thus subnets can be formed, further deepening a Submachine model.

There's a DNS repo which resolves in-game addresses to urls.
Levels can be dependent on each other and provide or request items from the player.
So that the whole thing becomes a giant puzzle with multiple ways of solving.
Items may be functional above the trivial key-lock model.
Like in metroids, some items provide additional ways of transport, thus enabling progression.

## Requirements for engine:

* Data-driven (levels should be loadable as data files without recompilation of game itself)
* Dynamically loading external files 
* Able to shellout or otherwise utilize git

### Not complying engines:

* Godot (can't load external files from host os? Uses virtual fs.)

### Complying engines