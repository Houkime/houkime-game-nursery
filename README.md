The thing with ideas is that they are rarely born ready and deep enough to 
start their implementation right away.
They also sometimes are born in too big quantities to handle all of them alone.
In a lifetime.

As a FOSS proponent I don't really plan to make profit by selling closed-source 
games and stuff. 
So there is actually no loss if some or all of these get implemented not by me.

Besides, it so happens that these potential games convey important messages
that will be quite sad if left unheard.

One more thing is about patent lawsuits being rabid recently.
Some of these files contain (or will contain) my irl technical considerations and I 
really want sth to be long-public if some company decides that they can cover 
everything in protective patenting like they like to do these days.