## AbstractSpace

X-like spacesim.
Ever wanted to use your favourite math package as a game engine?
Ever wanted to fly your spaceship from a terminal (inside the ship)?
Ever wanted a spacesim economy that makes sense?
Ever wanted a modern-looking foss spacesim?

### Key design decisions:
* No bloat. Pure minimalism. I mean it.
* I mean, no soundfiles, no textures, no even stls. Just code.
* Procedural sound using builtin tracker (derived from klystrack)
* Vulkan rendering
* Executable size under 30M.
* 3 races: Cubes, Spheres, and Cylinders. (and one hidden - Tetrahedrons)
* Procedural geometry of ships and stations
* Walkable interiors without stupid minigames
* No plot
* No obligatory crew on stations and ships
* No people on factories and freighters
* Every mission is an action (a number of missions is probably small but they all matter)
* Every action has consequences (no instancing or decorative stuff)

### possible Vulkan backends/sources:

* https://github.com/pniekamp/datum
* https://github.com/rukai/vulkano-text (for terminal probably)
* https://github.com/zeux/niagara
* https://github.com/WindyDarian/Vulkan-Forward-Plus-Renderer
* https://github.com/Themaister/Granite
* https://github.com/NovaMods/nova-renderer
* https://github.com/jian-ru/laugh_engine
* https://github.com/omni-viral/rendy
* https://github.com/kaleido3d/kaleido3d
* https://github.com/godlikepanos/anki-3d-engine
* https://github.com/LukasBanana/LLGL
* https://github.com/GameTechDev/stardust_vulkan
* https://github.com/trungtle/TLVulkanRenderer

THEY ARE ALL BLOATED CRAP!
-> Please, inspect their sources and derive a minimalistic version.
